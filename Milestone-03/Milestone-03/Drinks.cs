﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_03
{
    public class Drinks
    {
        public string DrinkName { get; set; }
        public int DrinkPrice = 5;

        private static Dictionary<int, string> AllDrinks = new Dictionary<int, string>
        {
            {1, "Coca-Cola"},
            {2, "Sprite"},
            {3, "Fanta"},
            {4, "7 Up"},
            {5, "Mountain Dew"},
            {6, "L & P"},
        };

        public void Drink(string Name)
        {
            DrinkName = Name;
        }

        public int GetPosition(int DrinkKey, ref string DrinkType)
        {
            if (AllDrinks.ContainsKey(DrinkKey))
            {
                DrinkName = AllDrinks[DrinkKey];
                DrinkType = DrinkName;
                return DrinkKey;
            }

            return -1;
        }

    }
}
