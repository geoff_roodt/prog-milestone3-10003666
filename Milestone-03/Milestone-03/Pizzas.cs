﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_03
{
    enum Pizza_Size
    {
        small = 1,
        medium = 2,
        large = 3
    }

    enum Pizza_Price
    {
        Small = 5,
        Medium = 7,
        Large = 10
    }

    class Pizzas
    {
        public string PizzaType { get; set; }

        public Pizza_Size PizzaSize { get; set; }

        public int PizzaPrice { get; set; }

        private static Dictionary<int, string> AllPizzaTypes = new Dictionary<int, string>
        {
            {1, "Meat Lover" },
            {2, "Hawaii" },
            {3, "Vegeterian" },
            {4, "Chicken Cranberry Camembert" },
            {5, "Mr Wedge" },
            {6, "Champagne Hame" },
            {7, "Seafood" },
            {8, "Beef and Onion" }
        };

        public void Pizza(string Type, Pizza_Size Size)
        {
            PizzaType = Type;
            PizzaSize = Size;

            SetPizzaPrice(Size);

        }

        public int GetPosition(int PizzaKey, ref string PizzaType)
        {
            if (AllPizzaTypes.ContainsKey(PizzaKey))
            {
                PizzaType = AllPizzaTypes[PizzaKey];
                return PizzaKey;
            }
            
            return -1;
        }

        public void SetPizzaSize(string Size)
        {
            Pizza_Size MySize;
            if (Enum.TryParse(Size.ToLower(), out MySize))
            {
                PizzaSize = MySize;
                SetPizzaPrice(PizzaSize);
            }

        }

        private void SetPizzaPrice(Pizza_Size Size)
        {
            switch (Size)
            {
                case Pizza_Size.small:
                    PizzaPrice = (int)Pizza_Price.Small;
                    break;

                case Pizza_Size.medium:
                    PizzaPrice = (int)Pizza_Price.Medium;
                    break;

                case Pizza_Size.large:
                    PizzaPrice = (int)Pizza_Price.Large;
                    break;
            }

        }

    }
}
