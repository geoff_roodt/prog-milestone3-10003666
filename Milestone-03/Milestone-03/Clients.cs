﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Milestone_03
{
    class Clients
    {
        public string ClientName { get; set; }
        public int ClientContact { get; set; }

        public void Client(string Name, int Contact)
        {
            ClientName = Name;
            ClientContact = Contact;
        }

    }
}
