﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Milestone_03
{
    class Milestone03
    {
        private static List<Pizzas> _AvailablePizzas;
        private static List<Drinks> _AvailableDrinks;
        private static Clients _MyClient;

        #region Methods

        /// <summary>
        /// The main method used to call to submethods in order to run the application
        /// </summary>
        /// <param name="args">Not required</param>
        private static void Main(string[] args)
        {
            var noCrash = false;
            while (!noCrash)
            {
                try
                {
                    _AvailableDrinks = new List<Drinks>();
                    _AvailablePizzas = new List<Pizzas>();
                    _MyClient = new Clients();

                    var orderedPizzas = new List<Pizzas>();
                    var orderedDrinks = new List<Drinks>();

                    CreateDrinks();
                    CreatePizzas();

                    orderedPizzas = OrderPizzas();
                    orderedDrinks = OrderDrinks();

                    ReviewOrder(orderedPizzas, orderedDrinks);
                    noCrash = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("An Exception Occured! You Have Been Taken Back to the Main Menu!!");
                    noCrash = false;
                }

            }
            Console.ReadLine();
        }

        /// <summary>
        /// The method that gets the client details and orders pizzas if needed
        /// </summary>
        /// <param name="orderedPizzas">referenced list to be modified</param>
        private static void GetClientDetails(ref List<Pizzas> orderedPizzas)
        {
            var dispLine = new string('*', 60);
            var myName = string.Empty;
            var validEntry = false;
            var phoneNum = 0;

            Console.WriteLine(dispLine);
            Console.WriteLine();

            while (!validEntry)
            {
                Console.WriteLine("What is Your Name?");
                myName = Console.ReadLine();

                if (!string.IsNullOrEmpty(myName))
                {
                    validEntry = true;
                }
                else
                {
                    Console.WriteLine("Please Enter a Name!");
                }
            }

            validEntry = false;
            while (!validEntry)
            {
                Console.WriteLine("And What is Your Phone Number?");
                if (int.TryParse(Console.ReadLine(), out phoneNum))
                {
                    validEntry = true;
                    _MyClient.ClientName = myName;
                    _MyClient.ClientContact = phoneNum;

                    Console.WriteLine($"Thank you {myName}!");

                    if (orderedPizzas.Count > 0)
                    {
                        return;
                    }

                    Console.WriteLine("You Will Now be Redirected to Order Your Pizzas!");
                    GetPizzaChoice(ref orderedPizzas);
                }
                else
                {
                    Console.WriteLine("Please Enter a Valid Number!");
                }
            }
        }

        /// <summary>
        /// Reviews the clinet order and displays totals and ordered content.
        /// Confirms/gets the client details if they do not exitst
        /// </summary>
        /// <param name="orderedPizzas">List of pizzas that the client has ordered</param>
        /// <param name="ordererdDrinks">List of drinks that the client has ordered</param>
        private static void ReviewOrder(List<Pizzas> orderedPizzas, List<Drinks> ordererdDrinks)
        {
            var dispLine = new string('*', 60);
            var userInput = string.Empty;
            var validEntry = false;
            var totalPrice = 0;

            if (string.IsNullOrEmpty(_MyClient.ClientName))
            {
                GetClientDetails(ref orderedPizzas);
            }

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine(dispLine);
            Console.WriteLine("Please Check Your Details- Are They Correct?");
            Console.WriteLine($"Customer Name: {_MyClient.ClientName}");
            Console.WriteLine($"Customer Phone Number: {_MyClient.ClientContact}");

            while (!validEntry)
            {
                userInput = Console.ReadLine();
                if (userInput.ToLower() == "yes" || userInput.ToLower() == "no" || userInput == "1" || userInput == "2")
                {
                    if (userInput.ToLower() == "no" || userInput == "2")
                    {
                        GetClientDetails(ref orderedPizzas);
                    }

                    validEntry = true;
                }
                else
                {
                    Console.WriteLine("Invalid Entry! (yes / no)");
                }
            }

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Ordered Pizzas:");
            foreach (var myPizza in orderedPizzas)
            {
                Console.WriteLine($"{myPizza.PizzaType} - {myPizza.PizzaSize}");
                totalPrice += myPizza.PizzaPrice;
            }
            Console.WriteLine();

            Console.WriteLine("Ordered Drinks:");
            foreach (var myDrink in ordererdDrinks)
            {
                Console.WriteLine($"{myDrink.DrinkName}");
                totalPrice += myDrink.DrinkPrice;
            }
            Console.WriteLine();

            Console.WriteLine(dispLine);
            Console.WriteLine(dispLine);
            Console.WriteLine();
            Console.WriteLine($"Total Pizzas: {orderedPizzas.Count}");
            Console.WriteLine($"Total Drinks: {ordererdDrinks.Count}");
            Console.WriteLine($"Total Price: {totalPrice.ToString("C")}");

            GetPayment(totalPrice);
        }


        #endregion

        #region Pizza

        /// <summary>
        /// Creates the pizzas that the client can order from
        /// </summary>
        private static void CreatePizzas()
        {
            var meatPizza = new Pizzas { PizzaType = "Meat Lover" };
            var hawaiinPizza = new Pizzas { PizzaType = "Hawaii" };
            var vegePizza = new Pizzas { PizzaType = "Vegeterian" };
            var cknCranCamPizza = new Pizzas { PizzaType = "Chicken Cranberry Camembert" };
            var mrWedge = new Pizzas { PizzaType = "Mr Wedge" };
            var champagneHam = new Pizzas { PizzaType = "Champagne Hame" };
            var seafoodPizza = new Pizzas { PizzaType = "Seafood" };
            var beefOnionPizza = new Pizzas { PizzaType = "Beef and Onion" };

            _AvailablePizzas.Add(meatPizza);
            _AvailablePizzas.Add(hawaiinPizza);
            _AvailablePizzas.Add(vegePizza);
            _AvailablePizzas.Add(cknCranCamPizza);
            _AvailablePizzas.Add(mrWedge);
            _AvailablePizzas.Add(champagneHam);
            _AvailablePizzas.Add(seafoodPizza);
            _AvailablePizzas.Add(beefOnionPizza);
        }

        /// <summary>
        /// Orders the clients pizzas, and calls off to sub methods to order drinks/get details
        /// </summary>
        /// <returns>The list of pizzas the client has ordered</returns>
        private static List<Pizzas> OrderPizzas()
        {
            var orderedPizzas = new List<Pizzas>();
            var dispLine = new string('*', 60);
            var validEntry = false;

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine(dispLine);
            Console.WriteLine("  Welcome to Jefe's Pizza!");
            Console.WriteLine("What would you like to order?");
            Console.WriteLine("1) Enter Name and Phone Number");
            Console.WriteLine("2) View Pizza List and Order");
            Console.WriteLine();

            while (!validEntry)
            {
                var userChoice = 0;
                if (int.TryParse(Console.ReadLine(), out userChoice))
                {
                    switch (userChoice)
                    {
                        case 1:
                            GetClientDetails(ref orderedPizzas);
                            validEntry = true;
                            break;

                        case 2:
                            GetPizzaChoice(ref orderedPizzas);
                            validEntry = true;
                            break;

                        default:
                            Console.WriteLine("Invalid Entry! Please Try Again:");
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Entry! Please Try Again:");
                }
            }

            return orderedPizzas;
        }

        /// <summary>
        /// Gets the client's pizza choices
        /// </summary>
        /// <param name="orderedPizzas">Modify the list of pizzas client has ordered</param>
        private static void GetPizzaChoice(ref List<Pizzas> orderedPizzas)
        {
            var dispLine = new string('*', 60);
            var userInput = string.Empty;
            var orderMore = false;

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("These are the Pizzas for Sale:");

            for (var i = 0; i < _AvailablePizzas.Count; i++)
            {
                Console.WriteLine($"{i + 1}) {_AvailablePizzas[i].PizzaType}");
            }
            Console.WriteLine(dispLine);

            while (!orderMore)
            {
                var validEntry = false;
                do
                {
                    Console.WriteLine("Please enter the name of the pizza you wish to order:");
                    userInput = Console.ReadLine();

                    if (string.IsNullOrEmpty(userInput))
                    {
                        Console.WriteLine("Invalid Entry!");
                    }
                    else
                    {
                        var numberSelection = -1;
                        if (int.TryParse(userInput, out numberSelection))
                        {
                            validEntry = _AvailablePizzas.Any(x => x.GetPosition(numberSelection, ref userInput) != -1);
                        }
                        else
                        {
                            validEntry = _AvailablePizzas.Any(x => x.PizzaType.ToLower() == userInput.ToLower());
                        }

                        if (validEntry)
                        {
                            orderedPizzas = GetPizzaSize(userInput, orderedPizzas);
                        }
                    }

                    if (validEntry)
                    {
                        var correctChoice = false;
                        Console.WriteLine("Would you like to order any more pizzas?");

                        while (!correctChoice)
                        {
                            userInput = Console.ReadLine().ToLower();
                            if (userInput.ToLower() != "yes" && userInput.ToLower() != "no" && userInput != "1" && userInput != "2")
                            {
                                Console.WriteLine("Please enter the correct choice (yes / no)");
                            }
                            else
                            {
                                correctChoice = true;
                                orderMore = userInput.ToLower() == "no" || userInput == "2";
                            }
                        }
                    }

                } while (!validEntry);
            }
        }

        /// <summary>
        /// Gets the client's pizza size
        /// </summary>
        /// <param name="userInput">The chosen pizza type</param>
        /// <param name="orderedPizzas">referenced list of ordered pizzas to be modified (sizes added)</param>
        private static List<Pizzas> GetPizzaSize(string userInput, List<Pizzas> orderedPizzas)
        {
            var validEntry = false;
            var mySize = string.Empty;
            var newPizza = new Pizzas();
            newPizza.PizzaType = _AvailablePizzas.FirstOrDefault(x => x.PizzaType.ToLower() == userInput.ToLower()).PizzaType;

            do
            {
                Console.WriteLine();
                Console.WriteLine($"What size would you like the {newPizza.PizzaType}? (small, medium, large)");
                mySize = Console.ReadLine();

                if (mySize == "1" || mySize == "2" || mySize == "3" || (!string.IsNullOrEmpty(mySize) && (mySize.ToLower() == "small" || mySize.ToLower() == "medium" || mySize.ToLower() == "large")))
                {
                    newPizza.SetPizzaSize(mySize);
                    orderedPizzas.Add(newPizza);
                    validEntry = true;
                }
                else
                {
                    Console.WriteLine("Invalid size choice");
                }

            } while (!validEntry);

            return orderedPizzas;
        }

        #endregion

        #region Drinks

        /// <summary>
        /// Creates the drinks the client can order from
        /// </summary>
        private static void CreateDrinks()
        {
            var Coke = new Drinks { DrinkName = "Coca-Cola" };
            var Sprite = new Drinks { DrinkName = "Sprite" };
            var Fanta = new Drinks { DrinkName = "Fanta" };
            var SevenUp = new Drinks { DrinkName = "7 Up" };
            var MountainDew = new Drinks { DrinkName = "Mountain Dew" };
            var LnP = new Drinks { DrinkName = "L & P" };

            _AvailableDrinks.Add(Coke);
            _AvailableDrinks.Add(Sprite);
            _AvailableDrinks.Add(Fanta);
            _AvailableDrinks.Add(SevenUp);
            _AvailableDrinks.Add(MountainDew);
            _AvailableDrinks.Add(LnP);
        }

        /// <summary>
        /// Orders the drinks for the client
        /// </summary>
        /// <returns>The list of ordered drinks</returns>
        private static List<Drinks> OrderDrinks()
        {
            var dispLine = new string('*', 60);
            var orderedDrinks = new List<Drinks>();

            var finishOrder = false;
            var userInput = string.Empty;


            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("Would You Like to Order Any Drinks?");

            while (!finishOrder)
            {
                userInput = Console.ReadLine();

                if (string.IsNullOrEmpty(userInput))
                {
                    Console.WriteLine("Invalid Entry!");
                }
                else if (userInput.ToLower() == "no" || userInput == "2")
                {
                    finishOrder = true;
                }
                else if (userInput.ToLower() == "yes" || userInput == "1")
                {
                    GetDrinkChoice(ref orderedDrinks);
                    finishOrder = true;
                    Console.WriteLine();
                }
            }

            return orderedDrinks;
        }

        /// <summary>
        /// Gets the client choice of drink
        /// </summary>
        /// <param name="orderedDrinks">referenced list of drinks to be modified with client choice (drinks are added)</param>
        private static void GetDrinkChoice(ref List<Drinks> orderedDrinks)
        {
            var dispLine = new string('*', 60);
            var userInput = string.Empty;
            var orderMore = false;

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine("What Drink Would You Like?");
            for (var i = 0; i < _AvailableDrinks.Count; i++)
            {
                Console.WriteLine($"{i + 1}) {_AvailableDrinks[i].DrinkName}");
            }
            Console.WriteLine(dispLine);

            while (!orderMore)
            {
                var validEntry = false;
                do
                {
                    Console.WriteLine("Please Enter the Name of the Drink You Wish to Order:");
                    userInput = Console.ReadLine();

                    if (string.IsNullOrEmpty(userInput))
                    {
                        Console.WriteLine("Invalid Entry!");
                    }
                    else
                    {
                        var numberSelection = -1;
                        if (int.TryParse(userInput, out numberSelection))
                        {
                            validEntry = _AvailableDrinks.Any(x => x.GetPosition(numberSelection, ref userInput) != -1);
                        }
                        else
                        {
                            validEntry = _AvailableDrinks.Any(x => x.DrinkName.ToLower() == userInput.ToLower());
                        }                        
                    }

                    if (validEntry)
                    {
                        var correctChoice = false;
                        var newDrink = new Drinks();

                        newDrink.DrinkName = _AvailableDrinks.FirstOrDefault(x => x.DrinkName.ToLower() == userInput.ToLower()).DrinkName;
                        orderedDrinks.Add(newDrink);

                        Console.WriteLine($"Would You Like to Order Any More Drinks? You Currently Have Ordered {orderedDrinks.Count} Drinks");

                        while (!correctChoice)
                        {
                            userInput = Console.ReadLine().ToLower();
                            if (userInput.ToLower() != "yes" && userInput.ToLower() != "no" && userInput != "1" && userInput != "2")
                            {
                                Console.WriteLine("Please enter the correct choice (yes / no)");
                            }
                            else
                            {
                                correctChoice = true;
                                orderMore = userInput.ToLower() == "no" || userInput == "2";
                            }
                        }
                    }

                } while (!validEntry);
            }
        }


        #endregion

        #region Payments

        /// <summary>
        /// Gets the payment method from the client
        /// </summary>
        /// <param name="TotalPrice">The total purchase price</param>
        private static void GetPayment(int TotalPrice)
        {
            var dispLine = new string('*', 60);
            var userInput = string.Empty;
            var validPayment = false;

            while (!validPayment)
            {
                Console.WriteLine(dispLine);
                Console.WriteLine("How Would You Like to Pay? Please choose 1/2/3/4 or 5");
                Console.WriteLine("1) $5");
                Console.WriteLine("2) $10");
                Console.WriteLine("3) $15");
                Console.WriteLine("4) $20");
                Console.WriteLine("5) Other Amount");
                userInput = Console.ReadLine();

                var paymentType = 0;
                if (int.TryParse(userInput, out paymentType))
                {
                    var paymentAmount = 0;
                    switch (paymentType)
                    {
                        case 1:
                            paymentAmount = 5;
                            break;
                        case 2:
                            paymentAmount = 10;
                            break;
                        case 3:
                            paymentAmount = 15;
                            break;
                        case 4:
                            paymentAmount = 20;
                            break;
                        case 5:
                            paymentAmount = -1;
                            break;
                    }

                    if (paymentAmount - TotalPrice >= 0 || paymentAmount == -1)
                    {
                        ProcessPayment(TotalPrice, paymentAmount);
                        validPayment = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine($"Your Payment Will Not be Accepted! You Need to Pay Atleast {TotalPrice.ToString("C")}!");
                    }
                }
                else
                {
                    Console.WriteLine("Please Choose Correctly!");
                    Console.WriteLine();
                }

            }
        }

        /// <summary>
        /// Processes the client's payment
        /// </summary>
        /// <param name="TotalPrice">The total purchase price</param>
        /// <param name="PaymentAmount">The amount the client has elected to pay</param>
        private static void ProcessPayment(int TotalPrice, int PaymentAmount)
        {
            var dispLine = new string('*', 60);

            if (PaymentAmount > 0)
            {
                CalculateChange(TotalPrice, PaymentAmount);
            }
            else
            {
                var validPayment = false;
                double payment = 0;

                while (!validPayment)
                {
                    Console.WriteLine();
                    Console.WriteLine(dispLine);
                    Console.WriteLine("Please Enter the Amount You Wish to Pay:");
                    if (double.TryParse(Console.ReadLine(), out payment))
                    {
                        if (payment - TotalPrice >= 0)
                        {
                            CalculateChange(TotalPrice, payment);
                            validPayment = true;
                        }
                        else
                        {
                            Console.WriteLine($"Please Pay At Least {TotalPrice.ToString("C")}");
                        }
                    }
                    else
                    {
                        Console.WriteLine("The Amount You Entered Was Incorrect, or Too Large");
                    }
                }
            }
        }

        /// <summary>
        /// Calculates the change the client is to receive
        /// </summary>
        /// <param name="TotalPrice">The total purchase price</param>
        /// <param name="PaymentAmount">The amount the client has elected to pay</param>
        private static void CalculateChange(int TotalPrice, int PaymentAmount)
        {
            CalculateChange(TotalPrice, double.Parse(PaymentAmount.ToString()));
        }

        /// <summary>
        /// Calculates the change the client is to receive
        /// </summary>
        /// <param name="TotalPrice">The total purchase price</param>
        /// <param name="PaymentAmount">The amount the client has elected to pay</param>
        private static void CalculateChange(int TotalPrice, double PaymentAmount)
        {
            var dispLine = new string('*', 60);
            decimal change = 0;            
            var modifiedPaymentAmount = decimal.Parse(PaymentAmount.ToString(), System.Globalization.NumberStyles.Float);
            var modifiedTotalPrice = decimal.Parse(TotalPrice.ToString());

            change = modifiedPaymentAmount - modifiedTotalPrice;

            Console.WriteLine();
            Console.WriteLine(dispLine);
            Console.WriteLine(change == 0 ? "Thank-you For Your Payment! You Will Not Receive Any Change." : $"Your Change is: {change.ToString("C")}; Thank-you For Your Payment!");
            Console.WriteLine();
            Console.WriteLine("Good Bye and Enjoy Your Pizzas!");
            Console.WriteLine(dispLine);
            Console.WriteLine(dispLine);
        }
        #endregion

    }
}
